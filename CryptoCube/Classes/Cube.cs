﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoCube.Classes
{
    class Cube
    {
        private char?[,,] cube;

        public char? this[int z, int y, int x]
        {
            get
            {
                if (z < 0 || z > 2 || y < 0 || z > 2 || x < 0 || x > 2)
                {
                    throw new IndexOutOfRangeException();
                }
                return cube[z, y, x];
            }
            private set
            {
                if (z < 0 || z > 2 || y < 0 || z > 2 || x < 0 || x > 2)
                {
                    throw new IndexOutOfRangeException();
                }
                cube[z, y, x] = value;
            }
        }

        public int Length { get; private set; }
        public Sequence Sequence { get; private set; }

        public Cube()
        {
            cube = new char?[2, 2, 2];
            Sequence = new Sequence();
        }

        public string Read()
        {
            string read = "";

            foreach(char? c in cube)
            {
                read += c;
            }

            return read;
        }

        public void Write(string text)
        {
            if (text.Length > 8)
            {
                throw new ArgumentException("Argument cannot have more than 8 chararacters.");
            }
            Length = text.Length;
            for(int index = 0; index < Length; index++)
            {
                Point3D p = Convert(index);
                cube[p.Z, p.Y, p.X] = text[index];
            }
        }

        private Point3D Convert(int index)
        {
            return new Point3D
            {
                X = index % 2,
                Y = (index / 2) % 2,
                Z = (index / 4) % 2
            };
        }

        public void Rotate(Direction direction)
        {
            Cube clone = this.Clone();

            for (int index = 0; index < cube.Length; index++)
            {
                Point3D orig = Convert(index);
                Point3D rot = Rotate(direction, orig);
                cube[rot.Z, rot.Y, rot.X] = clone[orig.Z, orig.Y, orig.X];
            }

            Sequence.Add(direction);
        }

        public void Rotate(Sequence directions)
        {
            foreach(Direction direction in directions)
            {
                Rotate(direction);
            }
        }

        public void ReverseRotate(Sequence directions)
        {
            for(int index = directions.Count - 1; index >= 0; index--)
            {
                ReverseRotate(directions[index]);
            }
        }

        public void ReverseRotate(Direction direction)
        {
            switch(direction)
            {
                case Direction.Up:
                    Rotate(Direction.Down);
                    break;
                case Direction.Right:
                    Rotate(Direction.Left);
                    break;
                case Direction.Down:
                    Rotate(Direction.Up);
                    break;
                case Direction.Left:
                    Rotate(Direction.Right);
                    break;
            }
        }

        public void Rotate(string sequenceString)
        {
            Sequence directions = new Sequence(sequenceString);
            Rotate(directions);
        }

        private Point3D Rotate(Direction direction, Point3D origin)
        {
            Point3D rotate = new Point3D();
            switch (direction)
            {
                case Direction.Up:
                    rotate.X = origin.X;
                    rotate.Y = (origin.Z + 1) % 2;
                    rotate.Z = origin.Y;
                    break;
                case Direction.Down:
                    rotate.X = origin.X;
                    rotate.Y = origin.Z;
                    rotate.Z = (origin.Y + 1) % 2;
                    break;
                case Direction.Left:
                    rotate.X = origin.Z;
                    rotate.Y = origin.Y;
                    rotate.Z = (origin.X + 1) % 2;
                    break;
                case Direction.Right:
                    rotate.X = (origin.Z + 1) % 2;
                    rotate.Y = origin.Y;
                    rotate.Z = origin.X;
                    break;
            }
            return rotate;
        }

        public Cube Clone()
        {
            Cube clone = new Cube();
            for (int index = 0; index < cube.Length; index++)
            {
                Point3D p = Convert(index);
                clone[p.Z, p.Y, p.X] = cube[p.Z, p.Y, p.X];
            }
            return clone;
        }

        public override string ToString()
        {
            return Read();
        }
    }
}
