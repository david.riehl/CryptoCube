﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoCube.Classes
{
    class CubeContainer : List<Cube>
    {
        public CubeContainer() : base() { }

        public string globalSequence
        {
            get
            {
                string str = "";
                if(Count > 0)
                {
                    for (int index = 0; index < this.Count; index++)
                    {
                        str += index + ":" + this[index].Sequence;
                        str += ",";
                    }
                    str = str.Substring(0, str.Length - 1);
                }
                return str;
            }
        }

        public void Crypte(string phrase)
        {
            for (int index = 0; index < phrase.Length; index += 8)
            {
                int diff = phrase.Length - index;
                string text;
                Cube cube = new Cube();
                if (diff > 8)
                {
                    text = phrase.Substring(index, 8);
                }
                else
                {
                    text = phrase.Substring(index, diff);
                }
                cube.Write(text);
                Sequence sequence = Sequence.CreateRandomSequence();
                Console.WriteLine("sequence : " + sequence);
                cube.Rotate(sequence);
                this.Add(cube);
            }
        }

        public void Decrypte(string phrase, string globalSequence)
        {
            List<Sequence> sequences = Sequence.CreateSequenceList(globalSequence);
            int cubeIndex = 0;
            for (int index = 0; index < phrase.Length; index += 8)
            {
                int diff = phrase.Length - index;
                string text;
                Cube cube = new Cube();
                if (diff > 8)
                {
                    text = phrase.Substring(index, 8);
                }
                else
                {
                    text = phrase.Substring(index, diff);
                }
                cube.Write(text);
                cube.ReverseRotate(sequences[cubeIndex]);
                this.Add(cube);
                cubeIndex++;
            }
        }

        public override string ToString()
        {
            string str = "";
            foreach(Cube cube in this)
            {
                str += cube;
            }
            return str;
        }
    }
}
